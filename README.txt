This module has two (2) benefits:
  1. better UI selection of terms. concept of the UI is similar to Hierarchical Select module.
  2. creates related content block using the terms selected (that's why the name). 
  
Better UI for selecting vocabularies
====================================
  The module allows you to select multiple vocabularies in a single field entity. In Hierarchical Select, you can only choose 1 vocabulary at a time. It still synced the terms selected in the taxonomy table so you can still use this in views.
  
  You can install this module just for this use.
  
  
Related Content Block
=====================
  This module produce a block that displays related content using terms. The logic is different from other similar modules (see related modules) when generating the results. This is for specific use case where in the terms assignment are properly designed by the editors or site owner. It takes into consideration the hierarchy of the tags and the order of the group they are selected.
  
  To better explain, here is an example. Let's assume we have Color as our only vocabulary assigned to a field entity. It has the following terms (written with hierarchy).
    # Red
      ## Light Red
      #### Light Red Combo 1
      #### Light Red Combo 2
      ## Dark Red
      ### Dark Red Combo 1
      ### Dark Red Combo 2
      ## Shiny Red
    # Blue
      ## Sky Blue
      #### Sky Blue Combo 1
      #### Sky Blue Combo 2
      ## Dark Blue
      ## Midnight Blue
    # Green
      ## Light Green
      #### Light Green Combo 1
      #### Light Green Combo 2
      ## Apple Green
      ## Dark Green
  
  So let's say we have the following articles with term assignments (note the order of how they are selected and grouped):
  Article1 are assigned the following terms: 
    Red > Light Red > Light Red Combo 2                         // This is called group 1
    Green > Light Green > Light Green Combo 1                   // This is group 2
  Article2 are assigned the following terms:
    Red > Light Red                                             // Group 1
    Red > Dark Red                                              // Group 2.
  Article3 are assigned the following terms:
    Red > Light Red > Light Red Combo 2
    Green > Light Green > Light Green Combo 1
  Article4 are assigned the following terms: 
    Red > Light Red
    Green > Dark Green
  Article5 are assigned the following terms: 
    Red > Light Red
    Green > Light Green > Light Green Combo 1
  Article6 are assigned the following terms: 
    Blue > Light Blue
    Red > Dark Red > Dark Red Combo 1
  
  If you are visiting Article1 and we only want to generate the top 3 related nodes, the following nodes will be displayed (in order)
  1. Article3 
  2. Article4
  3. Article5
  
  Why? Here is how they were selected:
  1. The search will start by looking for related contents marked in Group 1 only. It will start looking for nodes marked with "Red" and there are 6 articles. 
     // Result: All
  2. From this 6 articles, it will find nodes tagged "Light Red". There are 5 nodes marked "Light Red".
     // Result: Articles # 1,2,3,4,5
  3. From this 5 articles, it will find nodes tagged "Light Red Combo 2". There is only 1 node - Article 3. So this article is our #1 Related node.
  4. Since we need to find 2 more, it will continue the search by going down Group 2.
  5. From the 4 nodes from Step 2 (minus the selected Article3), it will search for nodes that are tagged with "Green". There are 2 nodes only
     // Result: Articles # 4,5
  6. Since there are only 2 nodes, that completes the search for the top 3. If there are more than 2, then it will continue to search for nodes tagged "Light Green" then "Light Green Combo 1"
  
  
  The first group dictates the overall relatedness of the nodes. This is only applicable if your content are well structured using taxonomy.
  
  This is sponsored by Summit Media Group and Promet Source.

Other modules that creates related content:
* See this document http://drupal.org/node/323329


SETTING UP THE FIELD
====================
  # Create vocabularies and terms to be able to use this.
  # Add a new field. Select "Multiple terms" for "Type of data to store.". Use the default "Select list" for "Form 
    element to edit the data."
    ## It doesn't have any field settings. In the edit page, make sure to select all the Vocabularies you want to 
       include here.
    ## Under the "Number of values", select "Unlimited". Otherwise, you'll get the error "this field cannot hold 
       more than 1 values"

  You are now ready to use feature when you create your node.


TO DISPLAY THE RELATED CONTENT BASED ON THE MATCHING TERMS
==========================================================
  By default, there is 1 block available for use. The name is "Relatedness". Configure this block as follows
  # "Field name to collect the terms" is intended if you have multiple fields that uses the field type
  # "Item count" is the number of items to display
  # "Display Options" are default options on how to display the items. There is another way to override this feature.
  # "Content type limit" if you only want to display specific content types for related nodes


ADD MORE BLOCKS
===============
  To add more blocks aside from the default block, go to Configuration > Content Authoring > Relatedness. 
  Select the additional number of blocks to create in "Number of blocks to create"


MODIFY SEARCH QUERY AND DISPLAY HOOKS
=====================================
  Below is a sample hooks on how to modify the search results and display of the block:

/**
 * Modify the query of the search. Add additional conditions to the query. 
 * Ex. I want to only show nodes that are not expired
 */
function modulename_relatedness_related_node_search($delta, $query) {
  if ($delta == 'relatedness_default') {
    // Should not be expired
    $fieldname = 'field_sponsor_expiration';
    $query->leftJoin('field_data_'.$fieldname, $fieldname, 'node.nid = '.$fieldname.'.entity_id 
  		AND node.vid = '.$fieldname.'.revision_id');
    $query->condition($fieldname.'_value', date('Y-m-d H:i:s'), '>=');
  }
  else if ($delta == 'relatedness_default_1') {
    // My next box should not be Sponsored
    $fieldname = 'field_term_subtype';
    $query->leftJoin('field_data_'.$fieldname, $fieldname, 'node.nid = '.$fieldname.'.entity_id 
  		AND node.vid = '.$fieldname.'.revision_id');
    $query->condition($fieldname.'_tid', 177, '!=');
  }
}

/**
 * Modify the display. I want to add an image and additional wrappers
 */
function modulename_process_relatedness_block(&$variables) {
  $imgdiv = false;
  $links = array();
  $nodes = $variables['nodes'];
  if ($nodes) {
    foreach ($nodes as $node) {
      $content = '';
      $imgdiv = false;
      
      if (isset($node->field_image[$node->language])) {
        $item = $node->field_image[$node->language][0];
  			
  			$image = array(
          'path' => $item['uri'],
          'alt' => $item['alt'],
        );
        
        $image['style_name'] = 'related-articles';
        $content = '<div class="custom-block-image">' . theme('image_style', $image) . '</div>';
        $imgdiv = true;
      }
      
      $content .= '<div class="custom-block-link">' . l($node->title, 'node/' . $node->nid) . '</div>';
      if($imgdiv){
        $content = '<div class="hasimage">'.$content.'</div>';
      }

      if ($variables['display_options'] == 'teaser') {
        $content = '<div class="custom-block-teaser">' . $node->teaser . '</div>';
      }
      
      $links[] =  '<div class="custom-block">' . $content . '</div>';
    }
    
    $variables['items'] = implode('', $links);
  }
  else {
    $variables['items'] = "";
  }
}


TODO
====
- Bug in field settings - need to consider default values and how to display them in the settings page (low priority)
- UI for "ajax is updating" type of functionality so the user knows if something is updating or not (nice to have)
- use form_set_value() for leaner code. see options_field_widget_validate() (nice to have)
